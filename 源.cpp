#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <graphics.h>//引用图形库头文件
#include <conio.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>

//定义图片
IMAGE zcd;//主菜单
IMAGE zcd_b;//主菜单点击效果
IMAGE gncd;//功能菜单
IMAGE zj_b;//增加功能点击效果
IMAGE sc_b;//删除功能点击效果
IMAGE xg_b;//修改功能点击效果
IMAGE cx_b;//查询功能点击效果
IMAGE px_b;//排序功能点击效果
IMAGE tj_b;//统计功能点击效果
IMAGE dy_b;//打印功能点击效果
IMAGE dq_b;//读取功能点击效果
IMAGE bc_b;//保存功能点击效果
IMAGE tc_b;//退出点击效果
IMAGE sc;//删除功能界面
IMAGE sc_xh;//按学号删除点击效果
IMAGE sc_xm;//按姓名删除点击效果
IMAGE sc_fh;//删除界面返回点击效果
IMAGE xg;//修改功能界面
IMAGE xg_xh;//修改学号点击效果
IMAGE xg_xm;//修改姓名点击效果
IMAGE xg_sx;//修改数学成绩点击效果
IMAGE xg_yy;//修改英语成绩点击效果
IMAGE xg_wl;//修改物理成绩点击效果
IMAGE xg_c;//修改C语言成绩点击效果
IMAGE xg_fh;//修改界面返回点击效果
IMAGE cx;//查询功能界面
IMAGE cx_xh;//按学号查询点击效果
IMAGE cx_xm;//按姓名查询点击效果
IMAGE cx_fh;//查询界面返回点击效果
IMAGE px;//排序功能界面
IMAGE px_sx;//按数学成绩排序点击效果
IMAGE px_yy;//按英语成绩排序点击效果
IMAGE px_wl;//按物理成绩排序点击效果
IMAGE px_c;//按C语言成绩排序点击效果
IMAGE px_fh;//排序界面返回点击效果
IMAGE px2;//排序二级界面
IMAGE px2_sx;//升序排序点击效果
IMAGE px2_jx;//降序排序点击效果
IMAGE px2_fh;//排序二级界面返回点击效果
IMAGE tj;//统计功能界面
IMAGE tj_sx;//统计数学成绩点击效果
IMAGE tj_yy;//统计英语成绩点击效果
IMAGE tj_wl;//统计物理成绩点击效果
IMAGE tj_c;//统计C语言成绩点击效果
IMAGE tj_fh;//统计界面返回点击效果
IMAGE dy;//打印功能界面
IMAGE dy_fh;//打印功能界面返回点击效果
IMAGE xg1;//修改方式选择界面
IMAGE xg1_xh;//按学号修改点击效果
IMAGE xg1_xm;//按姓名修改点击效果
IMAGE xg1_fh;//修改方式选择界面返回点击效果
IMAGE tc;//退出界面

//定义学生结构体
typedef struct student//代表学生信息的结构体
{
	wchar_t num[10];//学号
	wchar_t name[10];//姓名
	wchar_t shuxue[10];//数学成绩
	wchar_t yingyu[10];//英语成绩
	wchar_t cyuyan[10];//c语言成绩
	wchar_t wuli[10];//物理成绩
	struct student* next;//链表指针
}stu;

//函数声明
void menu(stu *head);//主菜单
void function(stu *head);//功能菜单
stu* initstu();//初始化链表
void addstu(stu* head);//增加学生信息
void deletestu(stu* head);//删除学生信息
void xgstu(stu* p,stu *head);//修改学生信息
void xgstu1(stu* head);
void searchstu(stu* head);//查询学生信息
void sortstu(stu* head);//排序主界面
void sortstu1(stu* head);//数学成绩排序
void sortstu2(stu* head);//英语成绩排序
void sortstu3(stu* head);//物理成绩排序
void sortstu4(stu* head);//C语言成绩排序
void tjstu(stu* head);//统计学生信息
void printstu(stu* head);//打印学生信息
void readstu(stu* head);//读取学生信息
void savestu(stu* head);//保存学生信息
int Wchar2Char(char* charStr, const wchar_t* wcharStr);//wchar_t转化为char
int Char2Wchar(wchar_t* wcharStr, const char* charStr); //char转化为wchar_t

//wchar_t与char的互化（摘自csdn，原文链接：https ://blog.csdn.net/bailang_zhizun/article/details/80348282）
int Wchar2Char(char* charStr, const wchar_t* wcharStr) //wchar_t转化为char
{
	int len = WideCharToMultiByte(CP_ACP, 0, wcharStr, wcslen(wcharStr), NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, wcharStr, wcslen(wcharStr), charStr, len, NULL, NULL);
	charStr[len] = '\0';
	return len;
}

int Char2Wchar(wchar_t* wcharStr, const char* charStr) //char转化为wchar_t
{
	int len = MultiByteToWideChar(CP_ACP, 0, charStr, strlen(charStr), NULL, 0);
	MultiByteToWideChar(CP_ACP, 0, charStr, strlen(charStr), wcharStr, len);
	wcharStr[len] = '\0';
	return len;
}

//主菜单函数
void menu(stu *head)
{
	loadimage(&zcd, _T("zcd.jpg"));
	putimage(0, 0, &zcd);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 774 && msg.x < 1024 && msg.y > 536 && msg.y < 647)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&zcd_b, _T("zcd_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &zcd_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}

//功能菜单函数
void function(stu *head)
{
	loadimage(&gncd, _T("gncd.jpg"));
	putimage(0, 0, &gncd);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 865 && msg.x < 1028 && msg.y > 35 && msg.y < 121)//鼠标点击特定区域，即“退出”所在区域
				{
					loadimage(&tc_b, _T("tc_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &tc_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					loadimage(&tc, _T("tc.jpg"));//导入退出界面
					putimage(0, 0, &tc);//显示退出界面
				}
				if (msg.x > 214 && msg.x < 409 && msg.y > 172 && msg.y < 275)//鼠标点击特定区域，即“增加学生信息”所在区域
				{
					loadimage(&zj_b, _T("zj_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &zj_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					addstu(head);
					function(head);
				}
				if (msg.x > 501 && msg.x < 697 && msg.y > 172 && msg.y < 275)//鼠标点击特定区域，即“删除学生信息”所在区域
				{
					loadimage(&sc_b, _T("sc_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &sc_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					deletestu(head);
				}
				if (msg.x > 783 && msg.x < 979 && msg.y > 172 && msg.y < 275)//鼠标点击特定区域，即“修改学生信息”所在区域
				{
					loadimage(&xg_b, _T("xg_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					xgstu1(head);
				}
				if (msg.x > 214 && msg.x < 408 && msg.y > 359 && msg.y < 461)//鼠标点击特定区域，即“查询学生信息”所在区域
				{
					loadimage(&cx_b, _T("cx_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &cx_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					searchstu(head);
				}
				if (msg.x > 501 && msg.x < 695 && msg.y > 359 && msg.y < 461)//鼠标点击特定区域，即“排序学生信息”所在区域
				{
					loadimage(&px_b, _T("px_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					sortstu(head);
				}
				if (msg.x > 784 && msg.x < 978 && msg.y > 359 && msg.y < 461)//鼠标点击特定区域，即“统计学生信息”所在区域
				{
					loadimage(&tj_b, _T("tj_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &tj_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					tjstu(head);
				}
				if (msg.x > 784 && msg.x < 978 && msg.y > 525 && msg.y < 628)//鼠标点击特定区域，即“保存学生信息”所在区域
				{
					loadimage(&bc_b, _T("bc_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &bc_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					savestu(head);
					function(head);
				}
				if (msg.x > 502 && msg.x < 695 && msg.y > 525 && msg.y < 628)//鼠标点击特定区域，即“读取学生信息”所在区域
				{
					loadimage(&dq_b, _T("dq_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &dq_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					readstu(head);
					function(head);
				}
				if (msg.x > 213 && msg.x < 409 && msg.y > 526 && msg.y < 629)//鼠标点击特定区域，即“打印学生信息”所在区域
				{
					loadimage(&dy_b, _T("dy_b.jpg"));//导入橙色按钮图片
					putimage(0, 0, &dy_b);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					printstu(head);
					function(head);
				}
			}
		}
	}
}

//初始化链表
stu* initstu()
{
	stu* head = (stu*)malloc(sizeof(stu));
	head->next = NULL;
	return head;
}

//增加学生信息
void addstu(stu* head)
{
	stu* p = NULL;
	p = (stu*)malloc(sizeof(stu));
	InputBox(p->num, 20, _T("请输入学号"));
	while (wcscmp(p->num, _T("")) == 0)
	{
		MessageBox(NULL, _T("输入不能为空！"), _T("增加学生信息"), MB_SETFOREGROUND);
		InputBox(p->num, 20, _T("请输入学号"));
	}
	InputBox(p->name, 20, _T("请输入姓名"));
	while (wcscmp(p->name, _T("")) == 0)
	{
		MessageBox(NULL, _T("输入不能为空！"), _T("增加学生信息"), MB_SETFOREGROUND);
		InputBox(p->name, 20, _T("请输入姓名"));
	}
	InputBox(p->shuxue, 10, _T("请输入数学成绩"));
	while (wcscmp(p->shuxue, _T("")) == 0)
	{
		MessageBox(NULL, _T("输入不能为空！"), _T("增加学生信息"), MB_SETFOREGROUND);
		InputBox(p->shuxue, 10, _T("请输入数学成绩"));
	}
	InputBox(p->yingyu, 10, _T("请输入英语成绩"));
	while (wcscmp(p->yingyu, _T("")) == 0)
	{
		MessageBox(NULL, _T("输入不能为空！"), _T("增加学生信息"), MB_SETFOREGROUND);
		InputBox(p->yingyu, 10, _T("请输入英语成绩"));
	}
	InputBox(p->wuli, 10, _T("请输入物理成绩"));
	while (wcscmp(p->wuli, _T("")) == 0)
	{
		MessageBox(NULL, _T("输入不能为空！"), _T("增加学生信息"), MB_SETFOREGROUND);
		InputBox(p->wuli, 10, _T("请输入物理成绩"));
	}
	InputBox(p->cyuyan, 10, _T("请输入C语言成绩")); 
	while (wcscmp(p->cyuyan, _T("")) == 0)
	{
		MessageBox(NULL, _T("输入不能为空！"), _T("增加学生信息"), MB_SETFOREGROUND);
		InputBox(p->cyuyan, 10, _T("请输入C语言成绩"));
	}
	p->next = head->next;
	head->next = p;
	MessageBox(NULL, _T("增加成功！"), _T("增加学生信息"), MB_SETFOREGROUND);
}

//打印学生信息
void printstu(stu* head)
{
	stu* p = head->next;
	int textx = 157;
	int texty = 123;
	loadimage(&dy, _T("dy.jpg"));
	putimage(0, 0, &dy);
	setbkmode(TRANSPARENT);//设置字体背景为透明
	settextcolor(COLORREF(RGB(0, 0, 0)));//设置字体颜色为黑色
	settextstyle(20, 0, _T("宋体"));//设置字体大小20，格式宋体
	outtextxy(157, 123, _T("学号            姓名             数学成绩            英语成绩            物理成绩            C语言成绩"));
	while(p!=NULL)
	{
		texty += 20;
		outtextxy(157, texty, p->num);
		outtextxy(317, texty, p->name);
		outtextxy(487, texty, p->shuxue);
		outtextxy(687, texty, p->yingyu);
		outtextxy(887, texty, p->wuli);
		outtextxy(1087, texty, p->cyuyan);
		p = p->next;
	}
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 1017 && msg.x < 1182 && msg.y > 27 && msg.y < 101)//鼠标点击特定区域，即返回按钮所在区域
				{
					loadimage(&dy_fh, _T("dy_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &dy_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}

//删除学生信息
void deletestu(stu* head)
{
	wchar_t str[21];
	stu* p1 = head, * p2 = head->next;
	loadimage(&sc, _T("sc.jpg"));
	putimage(0, 0, &sc);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 346 && msg.x < 852 && msg.y > 171 && msg.y < 310)//鼠标点击特定区域，即按学号删除按钮所在区域
				{
					loadimage(&sc_xh, _T("sc_xh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &sc_xh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(str, 20, _T("请输入需要删除的学生学号"));
					while (p2 != NULL)
					{
						if (wcscmp(p2->num, str) != 0)
						{
							p1 = p1->next;
							p2 = p2->next;
							continue;
						}
						else
						{
							p1->next = p2->next;
							free(p2);
							MessageBox(NULL, _T("删除成功！"), _T("删除学生信息"), MB_SETFOREGROUND);
							break;
						}
					}
					if (p2 == NULL) MessageBox(NULL, _T("未找到需要删除的学生信息！"), _T("删除学生信息"), MB_SETFOREGROUND);
					function(head);
				}
				if (msg.x > 346 && msg.x < 852 && msg.y > 374 && msg.y < 515)//鼠标点击特定区域，即按姓名删除按钮所在区域
				{
					loadimage(&sc_xm, _T("sc_xm.jpg"));//导入橙色按钮图片
					putimage(0, 0, &sc_xm);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(str, 20, _T("请输入需要删除的学生姓名"));
					while (p2 != NULL)
					{
 						if (wcscmp(p2->name, str) != 0)
						{
							p1 = p1->next;
							p2 = p2->next;
							continue;
						}
						else
						{
							p1->next = p2->next;
							free(p2);
							MessageBox(NULL, _T("删除成功！"), _T("删除学生信息"), MB_SETFOREGROUND);
							break;
						}
					}
					if (p2 == NULL) MessageBox(NULL, _T("未找到需要删除的学生信息！"), _T("删除学生信息"), MB_SETFOREGROUND);
					function(head);
				}
				if (msg.x > 850 && msg.x < 1037 && msg.y > 565 && msg.y < 677)//鼠标点击特定区域，即返回所在区域
				{
					loadimage(&sc_fh, _T("sc_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &sc_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}

//修改函数
void xgstu1(stu* head)
{
	stu* p = head->next;
	wchar_t str[21];
	loadimage(&xg1, _T("xg1.jpg"));
	putimage(0, 0, &xg1);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 345 && msg.x < 852 && msg.y > 169 && msg.y < 309)//鼠标点击特定区域，即按学号修改所在区域
				{
					loadimage(&xg1_xh, _T("xg1_xh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg1_xh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(str, 20, _T("请输入需要修改的学生学号"));
					while (p != NULL)
					{
						if (wcscmp(p->num, str) != 0)
						{
							p = p->next;
						}
						else
						{
							xgstu(p, head);
							break;
						}
					}
					if (p == NULL)
					{
						MessageBox(NULL, _T("未找到需要修改的学生信息！"), _T("修改学生信息"), MB_SETFOREGROUND);
						function(head);
					}
				}
				if (msg.x > 345 && msg.x < 852 && msg.y > 374 && msg.y < 515)//鼠标点击特定区域，即按姓名修改所在区域
				{
					loadimage(&xg1_xm, _T("xg1_xm.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg1_xm);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(str, 20, _T("请输入需要修改的学生姓名"));
					while (p != NULL)
					{
						if (wcscmp(p->name, str) != 0)
						{
							p = p->next;
						}
						else
						{
							xgstu(p, head);
							break;
						}
					}
					if (p == NULL)
					{
						MessageBox(NULL, _T("未找到需要修改的学生信息！"), _T("修改学生信息"), MB_SETFOREGROUND);
						function(head);
					}
				}
				if (msg.x > 850 && msg.x < 1037 && msg.y > 566 && msg.y < 679)//鼠标点击特定区域，即返回所在区域
				{
					loadimage(&xg1_fh,_T("xg1_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg1_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}
void xgstu(stu* p,stu *head)
{
	loadimage(&xg, _T("xg.jpg"));
	putimage(0, 0, &xg);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 239 && msg.x < 432 && msg.y > 180 && msg.y < 295)//鼠标点击特定区域，即学号按钮所在区域
				{
					loadimage(&xg_xh, _T("xg_xh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg_xh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(p->num, 20, _T("请输入新的学号"));
					MessageBox(NULL, _T("修改成功！"), _T("修改学生信息"), MB_SETFOREGROUND);
					function(head);
				}
				if (msg.x > 503 && msg.x < 694 && msg.y > 180 && msg.y < 295)//鼠标点击特定区域，即姓名按钮所在区域
				{
					loadimage(&xg_xm, _T("xg_xm.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg_xm);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(p->name, 20, _T("请输入新的姓名"));
					MessageBox(NULL, _T("修改成功！"), _T("修改学生信息"), MB_SETFOREGROUND);
					function(head);
				}
				if (msg.x > 767 && msg.x < 960 && msg.y > 180 && msg.y < 295)//鼠标点击特定区域，即数学成绩按钮所在区域
				{
					loadimage(&xg_sx, _T("xg_sx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg_sx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(p->shuxue, 20, _T("请输入新的数学成绩"));
					MessageBox(NULL, _T("修改成功！"), _T("修改学生信息"), MB_SETFOREGROUND);
					function(head);
				}
				if (msg.x > 239 && msg.x < 432 && msg.y > 390 && msg.y < 505)//鼠标点击特定区域，即英语成绩按钮所在区域
				{
					loadimage(&xg_yy, _T("xg_yy.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg_yy);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(p->yingyu, 20, _T("请输入新的英语成绩"));
					MessageBox(NULL, _T("修改成功！"), _T("修改学生信息"), MB_SETFOREGROUND);
					function(head);
				}
				if (msg.x > 503 && msg.x < 694 && msg.y > 390 && msg.y < 505)//鼠标点击特定区域，即物理成绩按钮所在区域
				{
					loadimage(&xg_wl, _T("xg_wl.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg_wl);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(p->wuli, 20, _T("请输入新的物理成绩"));
					MessageBox(NULL, _T("修改成功！"), _T("修改学生信息"), MB_SETFOREGROUND);
					function(head);
				}
				if (msg.x > 767 && msg.x < 960 && msg.y > 390 && msg.y < 505)//鼠标点击特定区域，即C语言成绩按钮所在区域
				{
					loadimage(&xg_c, _T("xg_c.jpg"));//导入橙色按钮图片
					putimage(0, 0, &xg_c);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(p->cyuyan, 20, _T("请输入新的C语言成绩"));
					MessageBox(NULL, _T("修改成功！"), _T("修改学生信息"), MB_SETFOREGROUND);
					function(head);
				}
			}
		}
	}
}

//查询函数
void searchstu(stu* head)
{
	stu* p = head->next;
	wchar_t str[21];
	loadimage(&cx, _T("cx.jpg"));
	putimage(0, 0, &cx);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 347 && msg.x < 852 && msg.y > 169 && msg.y < 311)//鼠标点击特定区域，即按学号查询按钮所在区域
				{
					loadimage(&cx_xh, _T("cx_xh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &cx_xh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(str, 20, _T("请输入需要查询的学生学号"));
					while (p != NULL)
					{
						if (wcscmp(p->num, str) != 0)
						{
							p = p->next;
						}
						else
						{
							MessageBox(NULL, _T("查询成功！"), _T("查询学生信息"), MB_SETFOREGROUND);
							loadimage(&dy, _T("dy.jpg"));
							putimage(0, 0, &dy);
							setbkmode(TRANSPARENT);//设置字体背景为透明
							settextcolor(COLORREF(RGB(0, 0, 0)));//设置字体颜色为黑色
							settextstyle(20, 0, _T("宋体"));//设置字体大小20，格式宋体
							outtextxy(157, 123, _T("学号            姓名             数学成绩            英语成绩            物理成绩            C语言成绩"));
							outtextxy(157, 143, p->num);
							outtextxy(317, 143, p->name);
							outtextxy(487, 143, p->shuxue);
							outtextxy(687, 143, p->yingyu);
							outtextxy(887, 143, p->wuli);
							outtextxy(1087, 143, p->cyuyan);
							MOUSEMSG msg1;//定义变量，保存鼠标消息
							FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
							while (true) // 主循环,循环监听鼠标信息
							{
								while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
								{
									msg1 = GetMouseMsg();//获取鼠标消息
									if (WM_LBUTTONDOWN == msg1.uMsg)//判断鼠标信息;鼠标左键按下
									{
										if (msg1.x > 1017 && msg1.x < 1182 && msg1.y > 27 && msg1.y < 101)//鼠标点击特定区域，即返回按钮所在区域
										{
											loadimage(&dy_fh, _T("dy_fh.jpg"));//导入橙色按钮图片
											putimage(0, 0, &dy_fh);//显示橙色按钮图片
											Sleep(100);//延时，降低CPU占用率，并且做到点击效果
											function(head);
										}
									}
								}
							}
						}
					}
					if (p == NULL)
					{
						MessageBox(NULL, _T("未找到需要查询的学生信息！"), _T("查询学生信息"), MB_SETFOREGROUND);
						function(head);
					}
				}
				if (msg.x > 347 && msg.x < 852 && msg.y > 374 && msg.y < 513)//鼠标点击特定区域，即按姓名查询按钮所在区域
				{
					loadimage(&cx_xm, _T("cx_xm.jpg"));//导入橙色按钮图片
					putimage(0, 0, &cx_xm);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(str, 20, _T("请输入需要查询的学生姓名"));
					while (p != NULL)
					{
						if (wcscmp(p->name, str) != 0)
						{
							p = p->next;
						}
						else
						{
							MessageBox(NULL, _T("查询成功！"), _T("查询学生信息"), MB_SETFOREGROUND);
							loadimage(&dy, _T("dy.jpg"));
							putimage(0, 0, &dy);
							setbkmode(TRANSPARENT);//设置字体背景为透明
							settextcolor(COLORREF(RGB(0, 0, 0)));//设置字体颜色为黑色
							settextstyle(20, 0, _T("宋体"));//设置字体大小20，格式宋体
							outtextxy(157, 123, _T("学号            姓名             数学成绩            英语成绩            物理成绩            C语言成绩"));
							outtextxy(157, 143, p->num);
							outtextxy(317, 143, p->name);
							outtextxy(487, 143, p->shuxue);
							outtextxy(687, 143, p->yingyu);
							outtextxy(887, 143, p->wuli);
							outtextxy(1087, 143, p->cyuyan);
							MOUSEMSG msg1;//定义变量，保存鼠标消息
							FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
							while (true) // 主循环,循环监听鼠标信息
							{
								while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
								{
									msg1 = GetMouseMsg();//获取鼠标消息
									if (WM_LBUTTONDOWN == msg1.uMsg)//判断鼠标信息;鼠标左键按下
									{
										if (msg1.x > 1017 && msg1.x < 1182 && msg1.y > 27 && msg1.y < 101)//鼠标点击特定区域，即返回按钮所在区域
										{
											loadimage(&dy_fh, _T("dy_fh.jpg"));//导入橙色按钮图片
											putimage(0, 0, &dy_fh);//显示橙色按钮图片
											Sleep(100);//延时，降低CPU占用率，并且做到点击效果
											function(head);
										}
									}
								}
							}
						}
					}
					if (p == NULL)
					{
						MessageBox(NULL, _T("未找到需要查询的学生信息！"), _T("查询学生信息"), MB_SETFOREGROUND);
						function(head);
					}
				}
				if (msg.x > 850 && msg.x < 1037 && msg.y > 566 && msg.y < 679)//鼠标点击特定区域，即返回所在区域
				{
					loadimage(&cx_fh, _T("cx_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &cx_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}

//排序函数
void sortstu(stu* head)
{
	stu* p = head->next;
	if (p == NULL)
	{
		MessageBox(NULL, _T("当前数据为空！"), _T("排序学生信息"), MB_SETFOREGROUND);
		function(head);
	}
	loadimage(&px, _T("px.jpg"));
	putimage(0, 0, &px);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 265 && msg.x < 532 && msg.y > 179 && msg.y < 322)//鼠标点击特定区域，即数学成绩按钮所在区域
				{
					loadimage(&px_sx, _T("px_sx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px_sx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					sortstu1(head);
				}
				if (msg.x > 665 && msg.x < 933 && msg.y > 179 && msg.y < 322)//鼠标点击特定区域，即英语成绩按钮所在区域
				{
					loadimage(&px_yy, _T("px_yy.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px_yy);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					sortstu2(head);
				}
				if (msg.x > 265 && msg.x < 532 && msg.y > 414 && msg.y < 558)//鼠标点击特定区域，即物理成绩按钮所在区域
				{
					loadimage(&px_wl, _T("px_wl.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px_wl);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					sortstu3(head);
				}
				if (msg.x > 665 && msg.x < 933 && msg.y > 414 && msg.y < 558)//鼠标点击特定区域，即C语言成绩按钮所在区域
				{
					loadimage(&px_c, _T("px_c.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px_c);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					sortstu4(head);
				}
				if (msg.x > 870 && msg.x < 1032 && msg.y > 575 && msg.y < 668)//鼠标点击特定区域，即进入返回按钮所在区域
				{
					loadimage(&px_fh, _T("px_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}
void sortstu1(stu* head)
{
	wchar_t str[21];
	int n = 0;
	stu* p = head->next;
	while (p != NULL)
	{
		n++;
		p = p->next;
	}
	p = head->next;
	loadimage(&px2, _T("px2.jpg"));
	putimage(0, 0, &px2);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 347 && msg.x < 852 && msg.y > 171 && msg.y < 311)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_sx, _T("px2_sx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_sx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					for (int k = 0; k < n - 1; k++)
					{
						for (int j = 1; j < n - k; j++)
						{
							if (_wtoi(p->shuxue) > _wtoi(p->next->shuxue))
							{
								wcscpy(str, p->num);
								wcscpy(p->num, p->next->num);
								wcscpy(p->next->num, str);
								wcscpy(str, p->name);
								wcscpy(p->name, p->next->name);
								wcscpy(p->next->name, str);
								wcscpy(str, p->shuxue);
								wcscpy(p->shuxue, p->next->shuxue);
								wcscpy(p->next->shuxue, str);
								wcscpy(str, p->yingyu);
								wcscpy(p->yingyu, p->next->yingyu);
								wcscpy(p->next->yingyu, str);
								wcscpy(str, p->wuli);
								wcscpy(p->wuli, p->next->wuli);
								wcscpy(p->next->wuli, str);
								wcscpy(str, p->cyuyan);
								wcscpy(p->cyuyan, p->next->cyuyan);
								wcscpy(p->next->cyuyan, str);
							}
							p = p->next;
						}
						p = head->next;
					}
					MessageBox(NULL, _T("排序成功！"), _T("排序学生信息"), MB_SETFOREGROUND);
					printstu(head);
				}
				if (msg.x > 347 && msg.x < 852 && msg.y > 374 && msg.y < 513)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_jx, _T("px2_jx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_jx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					for (int k = 0; k < n - 1; k++)
					{
						for (int j = 1; j < n - k; j++)
						{
							if (_wtoi(p->shuxue) < _wtoi(p->next->shuxue))
							{
								wcscpy(str, p->num);
								wcscpy(p->num, p->next->num);
								wcscpy(p->next->num, str);
								wcscpy(str, p->name);
								wcscpy(p->name, p->next->name);
								wcscpy(p->next->name, str);
								wcscpy(str, p->shuxue);
								wcscpy(p->shuxue, p->next->shuxue);
								wcscpy(p->next->shuxue, str);
								wcscpy(str, p->yingyu);
								wcscpy(p->yingyu, p->next->yingyu);
								wcscpy(p->next->yingyu, str);
								wcscpy(str, p->wuli);
								wcscpy(p->wuli, p->next->wuli);
								wcscpy(p->next->wuli, str);
								wcscpy(str, p->cyuyan);
								wcscpy(p->cyuyan, p->next->cyuyan);
								wcscpy(p->next->cyuyan, str);
							}
							p = p->next;
						}
						p = head->next;
					}
					MessageBox(NULL, _T("排序成功！"), _T("排序学生信息"), MB_SETFOREGROUND);
					printstu(head);
				}
				if (msg.x > 851 && msg.x < 1037 && msg.y > 566 && msg.y < 676)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_fh, _T("px2_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}
void sortstu2(stu* head)
{
	wchar_t str[21];
	int n = 0;
	stu* p = head->next;
	while (p != NULL)
	{
		n++;
		p = p->next;
	}
	p = head->next;
	loadimage(&px2, _T("px2.jpg"));
	putimage(0, 0, &px2);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 347 && msg.x < 852 && msg.y > 171 && msg.y < 311)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_sx, _T("px2_sx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_sx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					for (int k = 0; k < n - 1; k++)
					{
						for (int j = 1; j < n - k; j++)
						{
							if (_wtoi(p->yingyu) > _wtoi(p->next->yingyu))
							{
								wcscpy(str, p->num);
								wcscpy(p->num, p->next->num);
								wcscpy(p->next->num, str);
								wcscpy(str, p->name);
								wcscpy(p->name, p->next->name);
								wcscpy(p->next->name, str);
								wcscpy(str, p->shuxue);
								wcscpy(p->shuxue, p->next->shuxue);
								wcscpy(p->next->shuxue, str);
								wcscpy(str, p->yingyu);
								wcscpy(p->yingyu, p->next->yingyu);
								wcscpy(p->next->yingyu, str);
								wcscpy(str, p->wuli);
								wcscpy(p->wuli, p->next->wuli);
								wcscpy(p->next->wuli, str);
								wcscpy(str, p->cyuyan);
								wcscpy(p->cyuyan, p->next->cyuyan);
								wcscpy(p->next->cyuyan, str);
							}
							p = p->next;
						}
						p = head->next;
					}
					MessageBox(NULL, _T("排序成功!"), _T("排序学生信息"), MB_SETFOREGROUND);
					printstu(head);
				}
				if (msg.x > 347 && msg.x < 852 && msg.y > 374 && msg.y < 513)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_jx, _T("px2_jx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_jx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					for (int k = 0; k < n - 1; k++)
					{
						for (int j = 1; j < n - k; j++)
						{
							if (_wtoi(p->yingyu) < _wtoi(p->next->yingyu))
							{
								wcscpy(str, p->num);
								wcscpy(p->num, p->next->num);
								wcscpy(p->next->num, str);
								wcscpy(str, p->name);
								wcscpy(p->name, p->next->name);
								wcscpy(p->next->name, str);
								wcscpy(str, p->shuxue);
								wcscpy(p->shuxue, p->next->shuxue);
								wcscpy(p->next->shuxue, str);
								wcscpy(str, p->yingyu);
								wcscpy(p->yingyu, p->next->yingyu);
								wcscpy(p->next->yingyu, str);
								wcscpy(str, p->wuli);
								wcscpy(p->wuli, p->next->wuli);
								wcscpy(p->next->wuli, str);
								wcscpy(str, p->cyuyan);
								wcscpy(p->cyuyan, p->next->cyuyan);
								wcscpy(p->next->cyuyan, str);
							}
							p = p->next;
						}
						p = head->next;
					}
					MessageBox(NULL, _T("排序成功！"), _T("排序学生信息"), MB_SETFOREGROUND);
					printstu(head);
				}
				if (msg.x > 851 && msg.x < 1037 && msg.y > 566 && msg.y < 676)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_fh, _T("px2_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}
void sortstu3(stu* head)
{
	wchar_t str[21];
	int n = 0;
	stu* p = head->next;
	while (p != NULL)
	{
		n++;
		p = p->next;
	}
	p = head->next;
	loadimage(&px2, _T("px2.jpg"));
	putimage(0, 0, &px2);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 347 && msg.x < 852 && msg.y > 171 && msg.y < 311)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_sx, _T("px2_sx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_sx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					for (int k = 0; k < n - 1; k++)
					{
						for (int j = 1; j < n - k; j++)
						{
							if (_wtoi(p->wuli) > _wtoi(p->next->wuli))
							{
								wcscpy(str, p->num);
								wcscpy(p->num, p->next->num);
								wcscpy(p->next->num, str);
								wcscpy(str, p->name);
								wcscpy(p->name, p->next->name);
								wcscpy(p->next->name, str);
								wcscpy(str, p->shuxue);
								wcscpy(p->shuxue, p->next->shuxue);
								wcscpy(p->next->shuxue, str);
								wcscpy(str, p->yingyu);
								wcscpy(p->yingyu, p->next->yingyu);
								wcscpy(p->next->yingyu, str);
								wcscpy(str, p->wuli);
								wcscpy(p->wuli, p->next->wuli);
								wcscpy(p->next->wuli, str);
								wcscpy(str, p->cyuyan);
								wcscpy(p->cyuyan, p->next->cyuyan);
								wcscpy(p->next->cyuyan, str);
							}
							p = p->next;
						}
						p = head->next;
					}
					MessageBox(NULL, _T("排序成功！"), _T("排序学生信息"), MB_SETFOREGROUND);
					printstu(head);
				}
				if (msg.x > 347 && msg.x < 852 && msg.y > 374 && msg.y < 513)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_jx, _T("px2_jx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_jx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					for (int k = 0; k < n - 1; k++)
					{
						for (int j = 1; j < n - k; j++)
						{
							if (_wtoi(p->wuli) < _wtoi(p->next->wuli))
							{
								wcscpy(str, p->num);
								wcscpy(p->num, p->next->num);
								wcscpy(p->next->num, str);
								wcscpy(str, p->name);
								wcscpy(p->name, p->next->name);
								wcscpy(p->next->name, str);
								wcscpy(str, p->shuxue);
								wcscpy(p->shuxue, p->next->shuxue);
								wcscpy(p->next->shuxue, str);
								wcscpy(str, p->yingyu);
								wcscpy(p->yingyu, p->next->yingyu);
								wcscpy(p->next->yingyu, str);
								wcscpy(str, p->wuli);
								wcscpy(p->wuli, p->next->wuli);
								wcscpy(p->next->wuli, str);
								wcscpy(str, p->cyuyan);
								wcscpy(p->cyuyan, p->next->cyuyan);
								wcscpy(p->next->cyuyan, str);
							}
							p = p->next;
						}
						p = head->next;
					}
					MessageBox(NULL, _T("排序成功！"), _T("排序学生信息"), MB_SETFOREGROUND);
					printstu(head);
				}
				if (msg.x > 851 && msg.x < 1037 && msg.y > 566 && msg.y < 676)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_fh, _T("px2_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}
void sortstu4(stu* head)
{
	wchar_t str[21];
	int n = 0;
	stu* p = head->next;
	while (p != NULL)
	{
		n++;
		p = p->next;
	}
	p = head->next;
	loadimage(&px2, _T("px2.jpg"));
	putimage(0, 0, &px2);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 347 && msg.x < 852 && msg.y > 171 && msg.y < 311)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_sx, _T("px2_sx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_sx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					for (int k = 0; k < n - 1; k++)
					{
						for (int j = 1; j < n - k; j++)
						{
							if (_wtoi(p->cyuyan) > _wtoi(p->next->cyuyan))
							{
								wcscpy(str, p->num);
								wcscpy(p->num, p->next->num);
								wcscpy(p->next->num, str);
								wcscpy(str, p->name);
								wcscpy(p->name, p->next->name);
								wcscpy(p->next->name, str);
								wcscpy(str, p->shuxue);
								wcscpy(p->shuxue, p->next->shuxue);
								wcscpy(p->next->shuxue, str);
								wcscpy(str, p->yingyu);
								wcscpy(p->yingyu, p->next->yingyu);
								wcscpy(p->next->yingyu, str);
								wcscpy(str, p->wuli);
								wcscpy(p->wuli, p->next->wuli);
								wcscpy(p->next->wuli, str);
								wcscpy(str, p->cyuyan);
								wcscpy(p->cyuyan, p->next->cyuyan);
								wcscpy(p->next->cyuyan, str);
							}
							p = p->next;
						}
						p = head->next;
					}
					MessageBox(NULL, _T("排序成功！"), _T("排序学生信息"), MB_SETFOREGROUND);
					printstu(head);
				}
				if (msg.x > 347 && msg.x < 852 && msg.y > 374 && msg.y < 513)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_jx, _T("px2_jx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_jx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					for (int k = 0; k < n - 1; k++)
					{
						for (int j = 1; j < n - k; j++)
						{
							if (_wtoi(p->cyuyan) < _wtoi(p->next->cyuyan))
							{
								wcscpy(str, p->num);
								wcscpy(p->num, p->next->num);
								wcscpy(p->next->num, str);
								wcscpy(str, p->name);
								wcscpy(p->name, p->next->name);
								wcscpy(p->next->name, str);
								wcscpy(str, p->shuxue);
								wcscpy(p->shuxue, p->next->shuxue);
								wcscpy(p->next->shuxue, str);
								wcscpy(str, p->yingyu);
								wcscpy(p->yingyu, p->next->yingyu);
								wcscpy(p->next->yingyu, str);
								wcscpy(str, p->wuli);
								wcscpy(p->wuli, p->next->wuli);
								wcscpy(p->next->wuli, str);
								wcscpy(str, p->cyuyan);
								wcscpy(p->cyuyan, p->next->cyuyan);
								wcscpy(p->next->cyuyan, str);
							}
							p = p->next;
						}
						p = head->next;
					}
					MessageBox(NULL, _T("排序成功！"), _T("排序学生信息"), MB_SETFOREGROUND);
					printstu(head);
				}
				if (msg.x > 851 && msg.x < 1037 && msg.y > 566 && msg.y < 676)//鼠标点击特定区域，即进入系统按钮所在区域
				{
					loadimage(&px2_fh, _T("px2_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &px2_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}

//统计函数
void tjstu(stu* head)
{
	wchar_t a[21], b[21];
	stu* p = head->next;
	loadimage(&tj, _T("tj.jpg"));
	putimage(0, 0, &tj);
	MOUSEMSG msg;//定义变量，保存鼠标消息
	FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
	while (true) // 主循环,循环监听鼠标信息
	{
		while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
		{
			msg = GetMouseMsg();//获取鼠标消息
			if (WM_LBUTTONDOWN == msg.uMsg)//判断鼠标信息;鼠标左键按下
			{
				if (msg.x > 263 && msg.x < 532 && msg.y > 178 && msg.y < 322)//鼠标点击特定区域，即数学按钮所在区域
				{
					loadimage(&tj_sx, _T("tj_sx.jpg"));//导入橙色按钮图片
					putimage(0, 0, &tj_sx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(a, 20, _T("请输入所统计分数段的下限"));
					InputBox(b, 20, _T("请输入所统计分数段的上限"));
					loadimage(&dy, _T("dy.jpg"));
					putimage(0, 0, &dy);
					setbkmode(TRANSPARENT);//设置字体背景为透明
					settextcolor(COLORREF(RGB(0, 0, 0)));//设置字体颜色为黑色
					settextstyle(20, 0, _T("宋体"));//设置字体大小20，格式宋体
					outtextxy(157, 123, _T("学号            姓名             数学成绩            英语成绩            物理成绩            C语言成绩"));
					int textx = 157;
					int texty = 123;
					while (p != NULL)
					{
						if (_wtoi(p->shuxue) >= _wtoi(a) && _wtoi(p->shuxue) <= _wtoi(b))
						{
							texty += 20;
							outtextxy(157, texty, p->num);
							outtextxy(317, texty, p->name);
							outtextxy(487, texty, p->shuxue);
							outtextxy(687, texty, p->yingyu);
							outtextxy(887, texty, p->wuli);
							outtextxy(1087, texty, p->cyuyan);
						}
						p = p->next;
					}
					if (texty == 123)//texty没有增加说明没有添加信息，即当前信息中没有匹配的信息
					{
						MessageBox(NULL, _T("未找到相关学生信息！"), _T("统计学生信息"), MB_SETFOREGROUND);
						function(head);
					}
					MOUSEMSG msg1;//定义变量，保存鼠标消息
					FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
					while (true) // 主循环,循环监听鼠标信息
					{
						while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
						{
							msg1 = GetMouseMsg();//获取鼠标消息
							if (WM_LBUTTONDOWN == msg1.uMsg)//判断鼠标信息;鼠标左键按下
							{
								if (msg1.x > 1017 && msg1.x < 1182 && msg1.y > 27 && msg1.y < 101)//鼠标点击特定区域，即返回按钮所在区域
								{
									loadimage(&dy_fh, _T("dy_fh.jpg"));//导入橙色按钮图片
									putimage(0, 0, &dy_fh);//显示橙色按钮图片
									Sleep(100);//延时，降低CPU占用率，并且做到点击效果
									function(head);
								}
							}
						}
					}
				}
				if (msg.x > 665 && msg.x < 933 && msg.y > 178 && msg.y < 322)//鼠标点击特定区域，即数学按钮所在区域
				{
					loadimage(&tj_yy, _T("tj_yy.jpg"));//导入橙色按钮图片
					putimage(0, 0, &tj_yy);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(a, 20, _T("请输入所统计分数段的下限"));
					InputBox(b, 20, _T("请输入所统计分数段的上限"));
					loadimage(&dy, _T("dy.jpg"));
					putimage(0, 0, &dy);
					setbkmode(TRANSPARENT);//设置字体背景为透明
					settextcolor(COLORREF(RGB(0, 0, 0)));//设置字体颜色为黑色
					settextstyle(20, 0, _T("宋体"));//设置字体大小20，格式宋体
					outtextxy(157, 123, _T("学号            姓名             数学成绩            英语成绩            物理成绩            C语言成绩"));
					int textx = 157;
					int texty = 123;
					while (p != NULL)
					{
						if (_wtoi(p->yingyu) >= _wtoi(a) && _wtoi(p->yingyu) <= _wtoi(b))
						{
							texty += 20;
							outtextxy(157, texty, p->num);
							outtextxy(317, texty, p->name);
							outtextxy(487, texty, p->shuxue);
							outtextxy(687, texty, p->yingyu);
							outtextxy(887, texty, p->wuli);
							outtextxy(1087, texty, p->cyuyan);
						}
						p = p->next;
					}
					if (texty == 123)//texty没有增加说明没有添加信息，即当前信息中没有匹配的信息
					{
						MessageBox(NULL, _T("未找到相关学生信息！"), _T("统计学生信息"), MB_SETFOREGROUND);
						function(head);
					}
					MOUSEMSG msg1;//定义变量，保存鼠标消息
					FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
					while (true) // 主循环,循环监听鼠标信息
					{
						while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
						{
							msg1 = GetMouseMsg();//获取鼠标消息
							if (WM_LBUTTONDOWN == msg1.uMsg)//判断鼠标信息;鼠标左键按下
							{
								if (msg1.x > 1017 && msg1.x < 1182 && msg1.y > 27 && msg1.y < 101)//鼠标点击特定区域，即返回按钮所在区域
								{
									loadimage(&dy_fh, _T("dy_fh.jpg"));//导入橙色按钮图片
									putimage(0, 0, &dy_fh);//显示橙色按钮图片
									Sleep(100);//延时，降低CPU占用率，并且做到点击效果
									function(head);
								}
							}
						}
					}
				}
				if (msg.x > 263 && msg.x < 532 && msg.y > 414 && msg.y < 560)//鼠标点击特定区域，即数学按钮所在区域
				{
					loadimage(&tj_wl, _T("tj_wl.jpg"));//导入橙色按钮图片
					putimage(0, 0, &tj_sx);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(a, 20, _T("请输入所统计分数段的下限"));
					InputBox(b, 20, _T("请输入所统计分数段的上限"));
					loadimage(&dy, _T("dy.jpg"));
					putimage(0, 0, &dy);
					setbkmode(TRANSPARENT);//设置字体背景为透明
					settextcolor(COLORREF(RGB(0, 0, 0)));//设置字体颜色为黑色
					settextstyle(20, 0, _T("宋体"));//设置字体大小20，格式宋体
					outtextxy(157, 123, _T("学号            姓名             数学成绩            英语成绩            物理成绩            C语言成绩"));
					int textx = 157;
					int texty = 123;
					while (p != NULL)
					{
						if (_wtoi(p->wuli) >= _wtoi(a) && _wtoi(p->wuli) <= _wtoi(b))
						{
							texty += 20;
							outtextxy(157, texty, p->num);
							outtextxy(317, texty, p->name);
							outtextxy(487, texty, p->shuxue);
							outtextxy(687, texty, p->yingyu);
							outtextxy(887, texty, p->wuli);
							outtextxy(1087, texty, p->cyuyan);
						}
						p = p->next;
					}
					if (texty == 123)//texty没有增加说明没有添加信息，即当前信息中没有匹配的信息
					{
						MessageBox(NULL, _T("未找到相关学生信息！"), _T("统计学生信息"), MB_SETFOREGROUND);
						function(head);
					}
					MOUSEMSG msg1;//定义变量，保存鼠标消息
					FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
					while (true) // 主循环,循环监听鼠标信息
					{
						while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
						{
							msg1 = GetMouseMsg();//获取鼠标消息
							if (WM_LBUTTONDOWN == msg1.uMsg)//判断鼠标信息;鼠标左键按下
							{
								if (msg1.x > 1017 && msg1.x < 1182 && msg1.y > 27 && msg1.y < 101)//鼠标点击特定区域，即返回按钮所在区域
								{
									loadimage(&dy_fh, _T("dy_fh.jpg"));//导入橙色按钮图片
									putimage(0, 0, &dy_fh);//显示橙色按钮图片
									Sleep(100);//延时，降低CPU占用率，并且做到点击效果
									function(head);
								}
							}
						}
					}
				}
				if (msg.x > 666 && msg.x < 944 && msg.y > 414 && msg.y < 559)//鼠标点击特定区域，即数学按钮所在区域
				{
					loadimage(&tj_c, _T("tj_c.jpg"));//导入橙色按钮图片
					putimage(0, 0, &tj_c);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					InputBox(a, 20, _T("请输入所统计分数段的下限"));
					InputBox(b, 20, _T("请输入所统计分数段的上限"));
					loadimage(&dy, _T("dy.jpg"));
					putimage(0, 0, &dy);
					setbkmode(TRANSPARENT);//设置字体背景为透明
					settextcolor(COLORREF(RGB(0, 0, 0)));//设置字体颜色为黑色
					settextstyle(20, 0, _T("宋体"));//设置字体大小20，格式宋体
					outtextxy(157, 123, _T("学号            姓名             数学成绩            英语成绩            物理成绩            C语言成绩"));
					int textx = 157;
					int texty = 123;
					while (p != NULL)
					{
						if (_wtoi(p->shuxue) >= _wtoi(a) && _wtoi(p->shuxue) <= _wtoi(b))
						{
							texty += 20;
							outtextxy(157, texty, p->num);
							outtextxy(317, texty, p->name);
							outtextxy(487, texty, p->shuxue);
							outtextxy(687, texty, p->yingyu);
							outtextxy(887, texty, p->wuli);
							outtextxy(1087, texty, p->cyuyan);
						}
						p = p->next;
					}
					if (texty == 123)//texty没有增加说明没有添加信息，即当前信息中没有匹配的信息
					{
						MessageBox(NULL, _T("未找到相关学生信息！"), _T("统计学生信息"), MB_SETFOREGROUND);
						function(head);
					}
					MOUSEMSG msg1;//定义变量，保存鼠标消息
					FlushMouseMsgBuffer();// 清空鼠标消息缓冲区，避免无效鼠标信息带入到正式判断中
					while (true) // 主循环,循环监听鼠标信息
					{
						while (MouseHit())	//监听鼠标信息;当有鼠标消息的时候执行,可检测连续的鼠标信息
						{
							msg1 = GetMouseMsg();//获取鼠标消息
							if (WM_LBUTTONDOWN == msg1.uMsg)//判断鼠标信息;鼠标左键按下
							{
								if (msg1.x > 1017 && msg1.x < 1182 && msg1.y > 27 && msg1.y < 101)//鼠标点击特定区域，即返回按钮所在区域
								{
									loadimage(&dy_fh, _T("dy_fh.jpg"));//导入橙色按钮图片
									putimage(0, 0, &dy_fh);//显示橙色按钮图片
									Sleep(100);//延时，降低CPU占用率，并且做到点击效果
									function(head);
								}
							}
						}
					}
				}
				if (msg.x > 869 && msg.x < 1033 && msg.y > 575 && msg.y < 671)//鼠标点击特定区域，即数学按钮所在区域
				{
					loadimage(&tj_fh, _T("tj_fh.jpg"));//导入橙色按钮图片
					putimage(0, 0, &tj_fh);//显示橙色按钮图片
					Sleep(100);//延时，降低CPU占用率，并且做到点击效果
					function(head);
				}
			}
		}
	}
}
void savestu(stu* head)
{
	FILE* fp;
	stu* p = head->next;
	char num[1024];
	char name[1024];
	char shuxue[1024];
	char yingyu[1024];
	char wuli[1024];
	char cyuyan[1024];
	if (p == NULL)
	{
		MessageBox(NULL, _T("当前数据为空！"), _T("保存学生信息"), MB_SETFOREGROUND);
		return;
	}
	else
	{
		fp = fopen("data.txt", "w");
		fprintf(fp, "学号\t\t姓名\t\t数学\t\t英语\t\tC语言\t\t物理\n");
		while (p != NULL)
		{
			Wchar2Char(num, p->num);
			Wchar2Char(name, p->name);
			Wchar2Char(shuxue, p->shuxue);
			Wchar2Char(yingyu, p->yingyu);
			Wchar2Char(wuli, p->wuli);
			Wchar2Char(cyuyan, p->cyuyan);
			fprintf(fp, "%s\t\t%s\t\t%s\t\t%s\t\t%s\t\t%s\n", num, name, shuxue, yingyu, cyuyan, wuli);
			p = p->next;
		}
		fclose(fp);
		MessageBox(NULL, _T("保存成功！"), _T("保存学生信息"), MB_SETFOREGROUND);
	}
}
void readstu(stu* head)
{
	FILE* fp;
	stu* p = (stu*)malloc(sizeof(stu));
	char num[1024];
	char name[1024];
	char shuxue[1024];
	char yingyu[1024];
	char wuli[1024];
	char cyuyan[1024];
	fp = fopen("data.txt", "r");
	if (fp == NULL)
	{
		MessageBox(NULL, _T("读取失败！"), _T("读取学生信息"), MB_SETFOREGROUND);
		return;
	}
	fscanf(fp, "学号\t\t姓名\t\t数学\t\t英语\t\tC语言\t\t物理\n");
	while (fscanf(fp, "%s%s%s%s%s%s", num, name, shuxue, yingyu, cyuyan, wuli) != EOF)
	{
		Char2Wchar(p->num, num);
		Char2Wchar(p->name, name);
		Char2Wchar(p->shuxue, shuxue);
		Char2Wchar(p->yingyu, yingyu);
		Char2Wchar(p->cyuyan, cyuyan);
		Char2Wchar(p->wuli, wuli);
		p->next = head->next;
		head->next = p;
		p = (stu*)malloc(sizeof(stu));
	}
	free(p);
	fclose(fp);
	MessageBox(NULL, _T("读取成功！"), _T("读取学生信息"), MB_SETFOREGROUND);
}
int main()
{
	stu* head = initstu();
	initgraph(1200, 711);
	menu(head);
}
